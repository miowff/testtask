﻿using DotsBetweenSymbols;

Console.WriteLine("Write your string");
var input = Console.ReadLine();

while (string.IsNullOrWhiteSpace(input))
{
    Console.WriteLine("Wrong input\n try again");
    input = Console.ReadLine();
}


var stringHandler = new StringHandler(input);

try
{
    var arrayOfStrings = stringHandler.GetResult();
    foreach (var item in arrayOfStrings)
    {
        Console.WriteLine(item);
    }
}
catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}

