﻿
using System.Text;

namespace DotsBetweenSymbols
{
    public sealed class IndexesHandler
    {
        private readonly List<int> oddIndexes;
        public IndexesHandler(List<int> oddIndexes)
        {
            this.oddIndexes = oddIndexes;
        }
        /// <summary>
        /// This method finds all possible combos of indexes 
        /// The result are List of List<int>, the second contains all indexes for dots placement
        /// </summary>
        public List<List<int>> GetAllOddIndexesCombos(int numberSelectElements)
        {
            int[] indexesCombos = new int[numberSelectElements + 2];
            var result = new List<List<int>>();
            for (int i = 0; i < numberSelectElements; i++)
            {
                indexesCombos[i] = i;
            }
            indexesCombos[numberSelectElements] = oddIndexes.Count;
            indexesCombos[numberSelectElements + 1] = 0;
            while (true)
            {
                var indexes = IndexesToString(indexesCombos, 0, numberSelectElements);
                result.Add(GetSuitableOddIndexes(indexes));
                int j = 0;
                while (indexesCombos[j] + 1 == indexesCombos[j + 1])
                {
                    indexesCombos[j] = j;
                    j++;
                }
                if (j < numberSelectElements)
                {
                    indexesCombos[j]++;
                }
                else
                {
                    break;
                }
            }
            return result;
        }

        private string IndexesToString(int[] arr, int startIndex, int endIndex)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = startIndex; i <= endIndex - 1; i++)
            {
                sb.Append(arr[i]);
            }
            return sb.ToString();
        }

        private List<int> GetSuitableOddIndexes(string indexes)
        {
            List<int> result = new List<int>();
            foreach (var i in indexes)
            {
                result.Add(oddIndexes[(int)char.GetNumericValue(i)]);
            }
            return result;
        }
    }
}
