﻿namespace DotsBetweenSymbols
{
    public class StringHandler
    {
        private List<string> _result = new List<string>();
        private string _input;
        public StringHandler(string input)
        {
            this._input = input;
        }
        public string[] GetResult()
        {
            this._input = _input.Replace(" ", string.Empty);
            var charsInput = String.Join(" ", _input.ToCharArray()).ToCharArray();

            var maxPossibleDots = FindMaxPossibleDots(charsInput);

            if (maxPossibleDots == 0)
            {
                throw new ArgumentException("Impossible to place '.' between one symbol");
            }

            var oddIndexes = GetAllOddIndexes(charsInput);
            var indexesHandler = new IndexesHandler(oddIndexes);

            while (maxPossibleDots > 0)
            {
                var indexesForDots = indexesHandler.GetAllOddIndexesCombos(maxPossibleDots);
                SetDotsToIdexes(indexesForDots);
                maxPossibleDots--;
            }
            return _result.ToArray();
        }

        int FindMaxPossibleDots(char[] charsInput)
        {
            var maxDotsCount = 0;
            for (int i = 0; i < charsInput.Length; i++)
            {
                if (i + 1 != charsInput.Length && charsInput[i] == ' ')
                {
                    maxDotsCount++;
                }
            }
            return maxDotsCount;
        }
        /// <summary>
        /// This method finds all possible combos of indexes 
        /// </summary>
        List<int> GetAllOddIndexes(char[] charsInput)
        {
            var oddIndexes = new List<int>();
            for (int i = 1; i < charsInput.Length; i += 2)
            {
                oddIndexes.Add(i);
            }
            return oddIndexes;
        }

        void SetDotsToIdexes(List<List<int>> indexes)
        {
            foreach (var indexesList in indexes)
            {
                var charsInput = String.Join(" ", _input.ToCharArray()).ToCharArray();
                foreach (var index in indexesList)
                {
                    charsInput[index] = '.';
                }
                _result.Add(new string(charsInput).Replace(" ", ""));
            }
        }
    }
}
