namespace DotsBetweenSymbols.Tests
{
    public sealed class IndexesHandlerTests
    {
        private List<int> _oddIndexes = new List<int>() { 1, 3, 5 };
        private IndexesHandler _indexesHandler;
        [SetUp]
        public void Setup()
        {
            this._indexesHandler = new IndexesHandler(_oddIndexes);
        }

        [TestCase(3, 2)]
        [TestCase(3, 1)]
        [TestCase(1, 3)]
        public void IsGetAllOddindexesReturnsCorrectCount(int expectedCount, int countOfSelectedElements)
        {
            //act
            var result = _indexesHandler.GetAllOddIndexesCombos(countOfSelectedElements);
            //Assert
            Assert.AreEqual(expectedCount, result.Count);
        }
    }
}